﻿object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 359
  ClientWidth = 445
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Lb_Descrição: TLabel
    Left = 224
    Top = 70
    Width = 209
    Height = 13
    Caption = 'De dois cliques para escolher o personagem'
  end
  object ListaPerso: TListBox
    Left = 56
    Top = 8
    Width = 137
    Height = 217
    ItemHeight = 13
    TabOrder = 0
    OnDblClick = ListaPersoDblClick
  end
  object Botao1: TButton
    Left = 224
    Top = 8
    Width = 115
    Height = 25
    Caption = 'Personagens'
    TabOrder = 1
    OnClick = Botao1Click
  end
  object Botao2: TButton
    Left = 224
    Top = 39
    Width = 115
    Height = 25
    Caption = 'Novo Personagem'
    TabOrder = 2
    OnClick = Botao2Click
  end
  object NetHTTPClient1: TNetHTTPClient
    Asynchronous = False
    ConnectionTimeout = 60000
    ResponseTimeout = 60000
    AllowCookies = True
    HandleRedirects = True
    UserAgent = 'Embarcadero URI Client/1.0'
    Left = 56
    Top = 232
  end
end
